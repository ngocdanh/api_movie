import axios from 'axios'
import express from 'express'
import queryString from 'query-string'

const app = express.Router()
const baseUrl = 'https://api.themoviedb.org/3'

app.get('/:id', (req, res, next) => {
  const query = queryString.stringify(req.query);
	axios
		.all([
			axios.get(`${baseUrl}/person/${req.params.id}?api_key=${process.env.API_KEY}&${query}`),
			axios.get(
				`${baseUrl}/person/${req.params.id}/external_ids?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/person/${req.params.id}/images?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/person/${req.params.id}/tv_credits?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/person/${req.params.id}/movie_credits?api_key=${process.env.API_KEY}&${query}`
			),
		])
		.then(
			axios.spread((data, social, images, credits, similar) => {
				res.status(200).json({
					success: true,
					data: data.data,
					socialmedia: social.data,
					images: images.data,
					tv: credits.data,
					movies: similar.data,
				})
			})
		)
		.catch((err) => next(err))
})
export default app
