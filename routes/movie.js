import axios from 'axios'
import express from 'express'
import queryString from 'query-string'

const app = express.Router()
const baseUrl = 'https://api.themoviedb.org/3'

app.get('/:id', (req, res, next) => {
  const query = queryString.stringify(req.query);
	axios
		.all([
			axios.get(`${baseUrl}/movie/${req.params.id}?api_key=${process.env.API_KEY}&${query}`),
			axios.get(
				`${baseUrl}/movie/${req.params.id}/videos?api_key=${process.env.API_KEY}`
			),
			axios.get(
				`${baseUrl}/movie/${req.params.id}/images?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/movie/${req.params.id}/credits?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/movie/${req.params.id}/similar?api_key=${process.env.API_KEY}&${query}`
			),
		])
		.then(
			axios.spread((data, videos, images, credits, similar) => {
				res.status(200).json({
					success: true,
					data: data.data,
					videos: videos.data,
					images: images.data,
					credits: credits.data,
					similar: similar.data,
				})
			})
		)
		.catch((err) => next(err))
})

app.get('/omdb/:id', (req, res, next) => {
	axios
		.get(
			`http://www.omdbapi.com/?apikey=${process.env.OMDB_API_KEY}&i=${req.params.id}`
		)
		.then((data) => {
			res.status(200).json({
				success: true,

				data: data.data,
			})
		})
		.catch((err) => next(err))
})

export default app

// http://www.omdbapi.com/?i=$id&apikey=
