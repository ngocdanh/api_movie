import axios from 'axios'
import express from 'express'
import queryString from 'query-string'

const app = express.Router()
const baseUrl = 'https://api.themoviedb.org/3'

app.get('/:id/season/:snum', (req, res, next) => {
  const query = queryString.stringify(req.query)
	axios
		.all([
			axios.get(
				`${baseUrl}/tv/${req.params.id}/season/${req.params.snum}?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/tv/${req.params.id}/season/${req.params.snum}/videos?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/tv/${req.params.id}/season/${req.params.snum}/images?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/tv/${req.params.id}/season/${req.params.snum}/credits?api_key=${process.env.API_KEY}&language=en-US&include_image_language=en`
			),
		])
		.then(
			axios.spread((data, videos, images, credits, similar) => {
				res.status(200).json({
					success: true,
					data: data.data,
					videos: videos.data,
					images: images.data,
					credits: credits.data,
				})
			})
		)
		.catch((err) => next(err))
})
export default app
