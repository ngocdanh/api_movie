import axios from 'axios'
import express from 'express'
import queryString from 'query-string'
const app = express.Router()
const baseUrl = 'https://api.themoviedb.org/3'

app.get('/', (req, res, next) => {
  const query = queryString.stringify(req.query)
	axios
		.all([
			axios.get(`${baseUrl}/trending/movie/day?api_key=${process.env.API_KEY}&${query}`),
			axios.get(`${baseUrl}/movie/now_playing?api_key=${process.env.API_KEY}&${query}`),
			axios.get(
				`${baseUrl}/movie/top_rated?api_key=${process.env.API_KEY}&page=${Math.floor(
					Math.random() * 100
				)}`
			),
			axios.get(`${baseUrl}/trending/tv/day?api_key=${process.env.API_KEY}&${query}`),
			axios.get(
				`${baseUrl}/tv/top_rated?api_key=${process.env.API_KEY}&page=${Math.floor(
					Math.random() * 100
				)}&${query}`
			),
			axios.get(`${baseUrl}/movie/upcoming?api_key=${process.env.API_KEY}&${query}`),
		])
		.then(
			axios.spread(
				(trendm, nowPlaying, top_rated, trandtv, topRatedTv, upcoming) => {
					res.status(200).json({
						success: true,
						trandingMovies: trendm.data.results,
						nowPlayingMovies: nowPlaying.data.results,
						topRatedMovies: top_rated.data.results,
						trandingtv: trandtv.data.results,
						topRatedTv: topRatedTv.data.results,
						upcoming: upcoming.data.results,
					})
				}
			)
		)
		.catch((err) => next(err))
})

export default app
