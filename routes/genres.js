import express from 'express'
import axios from 'axios'
import queryString from 'query-string'
const app = express.Router()
const baseUrl = 'https://api.themoviedb.org/3'

app.get('/movie', (req, res, next) => {
  const query = queryString.stringify(req.query);
	axios
		.get(
			`${baseUrl}/genre/movie/list?api_key=${process.env.API_KEY}&${query}`
		)
		.then((data) => {
			res.status(200).json({
				success: true,
				data: data.data.genres,
			})
		})
		.catch((err) => next(err))
})

app.get('/tv', (req, res, next) => {
  const query = queryString.stringify(req.query);
	axios
		.get(
			`${baseUrl}/genre/tv/list?api_key=${process.env.API_KEY}&${query}`
		)
		.then((data) => {
			res.status(200).json({
				success: true,
				data: data.data.genres,
			})
		})
		.catch((err) => next(err))
})


export default app
