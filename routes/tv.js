import express from 'express'
import axios from 'axios'
const app = express.Router()
const baseUrl = 'https://api.themoviedb.org/3'

app.get('/:id', (req, res, next) => {
	axios
		.all([
			axios.get(`${baseUrl}/tv/${req.params.id}?api_key=${process.env.API_KEY}&${query}`),
			axios.get(`${baseUrl}/tv/${req.params.id}/videos?api_key=${process.env.API_KEY}&${query}`),
			axios.get(
				`${baseUrl}/tv/${req.params.id}/images?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/tv/${req.params.id}/credits?api_key=${process.env.API_KEY}&${query}`
			),
			axios.get(
				`${baseUrl}/tv/${req.params.id}/similar?api_key=${process.env.API_KEY}&${query}`
			),
		])
		.then(
			axios.spread((data, videos, images, credits, similar) => {
				res.status(200).json({
					success: true,
					data: data.data,
					videos: videos.data,
					images: images.data,
					credits: credits.data,
					similar: similar.data,
				})
			})
		)
		.catch((err) => next(err))
})

export default app
